﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace QLVT_DATHANG
{
    public partial class frmPhieuNhap : Form
    {
        String maCN = "";
        Boolean isEdit = true;
        String dgvMAVT_HT = "";

        public frmPhieuNhap()
        {
            InitializeComponent();
        }

        private void phieuNhapBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.bdsPN.EndEdit();
            this.tableAdapterManager.UpdateAll(this.DS);

        }

        public void sp_VattuNhap_dgvCombobox()
        {
            String sp_VattuDAT = "EXEC sp_VattuNhap '" + ((DataRowView)bdsPN[bdsPN.Position])["MasoDDH"].ToString() + "'";
            DataTable dt = new DataTable();
            dt = Program.ExecSqlDataTable(sp_VattuDAT);
            dgvMAVT.DataSource = dt;
            dgvMAVT.DisplayMember = "TENVT";
            dgvMAVT.ValueMember = "MAVT";
            
        }

        public void CTPN_dgvCombobox()
        {

            //HIỂN THỊ LẦN ĐÂU KHI CHƯA CLICK
            String vattuCTPN = "SELECT  MAVT FROM CTPN WHERE MAPN = '" + ((DataRowView)bdsPN[bdsPN.Position])["MAPN"].ToString() + "'";
            Program.myReader = Program.ExecSqlDataReader(vattuCTPN);
            if (Program.myReader == null)
            {
                return;
            }
            int rowCTPN = 0;
            while (Program.myReader.Read())
            {

                dgvCTPN.Rows[rowCTPN].Cells["dgvMAVT"].Value = Program.myReader.GetString(0);
                rowCTPN++;
            }
            Program.myReader.Close();
        }

        private void frmPhieuNhap_Load(object sender, EventArgs e)
        {
            DS.EnforceConstraints = false;

            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

            this.ctpnTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctpnTableAdapter.Fill(this.DS.CTPN);

            this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Fill(this.DS.DatHang);

            this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dsnvTableAdapter.Fill(this.DS.DSNV);

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.chuaNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.chuaNhapTableAdapter.Fill(this.DS.ChuaNhap);

            //cmb rẽ site
            maCN = ((DataRowView)bdsKho[0])["MACN"].ToString();

            cmbTenCN.DataSource = Program.bds_dspm;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedIndex = Program.mChinhanh;

            
            panelControl1.Enabled = false;
            dgvCTPN.Enabled = false;
            if (Program.mGroup == "CONGTY")
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = false;
            }
            else
            {
                cmbTenCN.Enabled = false;
            }

            //
            cmbMaDDH.SelectedValue = txbMaDDH.Text;
            cmbHOTEN.SelectedValue = txbMaNV.Text;
            cmbTENKHO.SelectedValue = txbMaKHO.Text;

            //sub form
            sp_VattuNhap_dgvCombobox();
            CTPN_dgvCombobox();
        }

        private void cmbTenCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTenCN.SelectedValue.ToString() == "System.Data.DataRowView")
            {
                return;
            }
            Program.servername = cmbTenCN.SelectedValue.ToString();

            if (cmbTenCN.SelectedIndex != Program.mChinhanh)
            {

                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }

            if (Program.KetNoi() == 0)
            {
                MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
            }
            else
            {
                this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
                this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

                this.ctpnTableAdapter.Connection.ConnectionString = Program.connstr;
                this.ctpnTableAdapter.Fill(this.DS.CTPN);

                this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.datHangTableAdapter.Fill(this.DS.DatHang);

                this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
                this.dsnvTableAdapter.Fill(this.DS.DSNV);

                this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
                this.khoTableAdapter.Fill(this.DS.Kho);

                maCN = ((DataRowView)bdsKho[0])["MACN"].ToString();
            }
        }

        private void cmbHOTEN_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txbMaNV.Text = cmbHOTEN.SelectedValue.ToString();
            }
            catch (Exception ex) { }
        }

        private void cmbTENKHO_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txbMaKHO.Text = cmbTENKHO.SelectedValue.ToString();
            }
            catch (Exception ex) { }
        }

        private void cmbMaDDH_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txbMaDDH.Text = cmbMaDDH.SelectedValue.ToString();
            }
            catch (Exception ex) { }
        }

        private void btnAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            cmbTENKHO.SelectedIndex = -1;
            //
            isEdit = false;
            //
            txbMaPN.Enabled = true;
            //
            gcPN.Enabled = false;
            //
            panelControl1.Enabled = true;
            //
            bdsPN.AddNew();
            //
            cmbHOTEN.Enabled = false;
            cmbHOTEN.SelectedValue = Program.username;
            txbMaNV.Text = Program.username;
            //
            btnAdd.Enabled = false;
            btnDelete.Enabled = btnEdit.Enabled = btnReLoad.Enabled = btnExit.Enabled = false;
            btnSave.Enabled = btnUndo.Enabled = true;
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            isEdit = true;
            cmbHOTEN.Enabled = true;
            txbMaPN.Enabled = false;
            panelControl1.Enabled = true;
            btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = false;
            btnSave.Enabled = btnUndo.Enabled = true;
            
        }

        private int sp_TimPN()
        {
            String sp_TimPN = "EXEC sp_TimPN '" + txbMaPN.Text.Trim() + "'";
            Program.myReader = Program.ExecSqlDataReader(sp_TimPN);
            if (Program.myReader != null)
            {
                MessageBox.Show("Mã phiếu nhập đã tồn tại", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaPN.Focus();
                Program.myReader.Close();
                return 1;
            }
            //Program.myReader.Close();
            return 0;
        }

        private int compareDay()
        {
            String sql = "SELECT NGAY FROM DatHang WHERE MasoDDH = '"+ txbMaDDH.Text + "'";
            Program.myReader = Program.ExecSqlDataReader(sql);
            if (Program.myReader == null)
            {
                MessageBox.Show("Không tìm thấy ngày của mã đơn đặt "+ txbMaDDH.Text, "THÔNG BÁO", MessageBoxButtons.OK);
                return 0;
            }
            Program.myReader.Read();
            DateTime ngayDat = Program.myReader.GetDateTime(0);
            Program.myReader.Close();
            return DateTime.Compare(Convert.ToDateTime(dptNGAY.Text), ngayDat); 
        }

        private void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (txbMaPN.Text.Trim() == "")
            {
                MessageBox.Show("Mã phiếu nhập không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaPN.Focus();
                return;
            }

            //ktra mã trùng
            if (!isEdit)
            {
                if(sp_TimPN() == 1)
                {
                    return;
                }
            }

            if(dptNGAY.Text.Trim() == "")
            {
                    MessageBox.Show("Ngày nhập không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                    dptNGAY.Focus();
                    return;
            }

            if(txbMaDDH.Text.Trim() == "")
            {
                MessageBox.Show("Mã đơn đặt không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaDDH.Focus();
                return;
            }

            if(txbMaNV.Text.Trim() == "")
            {
                MessageBox.Show("Nhân viên lập phiếu không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaNV.Focus();
                return;
            }

            if(txbMaKHO.Text.Trim() == "")
            {
                MessageBox.Show("Mã kho không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaKHO.Focus();
                return;
            }

            //so sánh ngày
            if(compareDay() < 0)
            {
                MessageBox.Show("Thời gian nhập không được trước thời gian đặt", "THÔNG BÁO", MessageBoxButtons.OK);
                dptNGAY.Focus();
                return;
            }

            try
            {
                bdsPN.EndEdit();
                bdsPN.ResetCurrentItem();
                this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
                this.phieuNhapTableAdapter.Update(this.DS.PhieuNhap);

                MessageBox.Show("Ghi thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                panelControl1.Enabled = false;
                btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = true;
                btnSave.Enabled = btnUndo.Enabled = true;
                gcPN.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi nhập hàng\n " + ex.Message, "", MessageBoxButtons.OK);
                panelControl1.Enabled = false;
                btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = true;
                btnSave.Enabled = btnUndo.Enabled = true;
                gcPN.Enabled = true;
                return;
            }

        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            panelControl1.Enabled = false;
            btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = false;

            DialogResult rs = MessageBox.Show("Bạn có muốn xóa phiếu nhập này không ?", "THÔNG BÁO", MessageBoxButtons.YesNo);
            
            if(DialogResult.Yes == rs)
            {
                if (bdsCTPN.Count > 0)
                {
                    MessageBox.Show("Phiếu nhập này không thể xóa !", "THÔNG BÁO", MessageBoxButtons.OK);

                    bdsPN.Position = 0;
                    panelControl1.Enabled = false;
                    btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                }
                else
                {
                    try
                    {
                        bdsPN.EndEdit();
                        bdsPN.RemoveCurrent();
                        this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
                        this.phieuNhapTableAdapter.Update(this.DS.PhieuNhap);


                        MessageBox.Show("Xóa thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                        bdsPN.Position = 0;
                        panelControl1.Enabled = false;
                        btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Xóa thất bại Lỗi: "+ex.Message, "THÔNG BÁO", MessageBoxButtons.OK);
                    }
                }    
            }
            else
            {
                panelControl1.Enabled = true;
                btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
            }
        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            panelControl1.Enabled = false;
            if (Program.mGroup == "CONGTY")
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = false;
                cmbTenCN.Enabled = btnReLoad.Enabled = true;
                gcPN.Enabled = true;
            }
            else
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = true;
                cmbTenCN.Enabled = false;
                gcPN.Enabled = true;
            }

            bdsPN.CancelEdit();
            bdsPN.EndEdit();
            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

            this.ctpnTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctpnTableAdapter.Fill(this.DS.CTPN);

            this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Fill(this.DS.DatHang);

            this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dsnvTableAdapter.Fill(this.DS.DSNV);

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.chuaNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.chuaNhapTableAdapter.Fill(this.DS.ChuaNhap);
        }

        private void btnReLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnExit.Enabled = panelControl1.Enabled = false;

            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

            this.ctpnTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctpnTableAdapter.Fill(this.DS.CTPN);

            this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Fill(this.DS.DatHang);

            this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dsnvTableAdapter.Fill(this.DS.DSNV);

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.chuaNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.chuaNhapTableAdapter.Fill(this.DS.ChuaNhap);

            MessageBox.Show("Tải lại dữ liệu thành công", "Thông báo", MessageBoxButtons.OK);


            btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
            panelControl1.Enabled = false;
            bdsPN.Position = 0;
            gcPN.Enabled = true;
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }


        /*contexMenuStrip */

        private int soLuong()
        {
            String soLuong = "SELECT SOLUONG FROM CTDDH WHERE MasoDDH = '" + ((DataRowView)bdsPN[bdsPN.Position])["MasoDDH"].ToString().Trim()
                         + "' and MAVT = '"+ dgvCTPN.CurrentRow.Cells["dgvMAVT"].Value.ToString().Trim()+"'";

            Program.myReader = Program.ExecSqlDataReader(soLuong);
            if(Program.myReader == null)
            {
                MessageBox.Show("Vui lòng kiểm tra chi tiết đặt hàng !", "THÔNG BÁO", MessageBoxButtons.OK);
                return 0;
            }
            
            Program.myReader.Read();
            int sl = Int32.Parse(Program.myReader.GetInt32(0).ToString());
            Program.myReader.Close();
            Program.conn.Close();
            return sl;
        }

        private int checkCTPN()
        {
            String checkCTPN = "SELECT MAPN, MAVT FROM CTPN WHERE MAVT='" + dgvCTPN.CurrentRow.Cells["dgvMAVT"].Value.ToString()
                                    + "' AND MAPN = '" + dgvCTPN.CurrentRow.Cells["dgvMAPN"].Value.ToString() + "'";
            Program.myReader = Program.ExecSqlDataReader(checkCTPN);
            if(Program.myReader.Read() == false)
            {
                Program.myReader.Close();
                return 1;
            }
            Program.myReader.Close();
            MessageBox.Show("Chi tiết đơn hàng này đã tồn tại !", "THÔNG BÁO", MessageBoxButtons.OK);
            return 0;
        }

        private void btnSaveCTPN_Click(object sender, EventArgs e)
        {

            if (dgvCTPN.CurrentRow.Cells["dgvMAVT"].Value.ToString().Trim() == "")
            {
                MessageBox.Show("Mã vật tư không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            if (dgvCTPN.CurrentRow.Cells["dgvSOLUONG"].Value.ToString().Trim() == "")
            {
                MessageBox.Show("Số lượng không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            if (Int32.Parse(dgvCTPN.CurrentRow.Cells["dgvSOLUONG"].Value.ToString()) > soLuong())
            {
                MessageBox.Show("Số lượng tối đa là :" + soLuong(), "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            if (dgvCTPN.CurrentRow.Cells["dgvDONGIA"].Value.ToString().Trim() == "")
            {
                MessageBox.Show("Đơn giá không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            if(dgvCTPN.CurrentRow.Cells["dgvMAVT"].Value.ToString() != dgvMAVT_HT && checkCTPN() == 0)
            {
                return;
            }

            try
            {

                bdsCTPN.EndEdit();
                bdsCTPN.ResetCurrentItem();
                this.ctpnTableAdapter.Connection.ConnectionString = Program.connstr;
                this.ctpnTableAdapter.Update(this.DS.CTPN);

                MessageBox.Show("Ghi thành công !", "THÔNG BÁO", MessageBoxButtons.OK);
                this.phieuNhapTableAdapter.Fill(DS.PhieuNhap);

                //bdsPN.Position = 0;
                dgvCTPN.Enabled = false;
                sp_VattuNhap_dgvCombobox();
                CTPN_dgvCombobox();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi nhập hàng\n " + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
        }

        private void btnDeleteCTPN_Click(object sender, EventArgs e)
        {
            //panelControl1.Enabled = false;
            //btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = false;

            DialogResult rs = MessageBox.Show("Bạn có muốn xóa phiếu nhập này không ?", "THÔNG BÁO", MessageBoxButtons.YesNo);

            if (DialogResult.Yes == rs)
            {
                try
                {
                    bdsCTPN.EndEdit();
                    bdsCTPN.RemoveCurrent();
                    this.ctpnTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.ctpnTableAdapter.Update(this.DS.CTPN);
                    this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

                    sp_VattuNhap_dgvCombobox();
                    CTPN_dgvCombobox();

                    MessageBox.Show("Xóa thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                    //bdsPN.Position = 0;
                    //panelControl1.Enabled = false;
                    //btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                   // bdsPN.Position = 0;
                    dgvCTPN.Enabled = false;
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Xóa thất bại Lỗi: " + ex.Message, "THÔNG BÁO", MessageBoxButtons.OK);
                    //bdsPN.Position = 0;
                    dgvCTPN.Enabled = false;
                    return;
                }
            }
            else
            {
                //panelControl1.Enabled = true;
                //btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                //bdsPN.Position = 0;
                dgvCTPN.Enabled = false;
            }
        }

        private void gcPN_Click(object sender, EventArgs e)
        {
            sp_VattuNhap_dgvCombobox();
            CTPN_dgvCombobox();
        }

        private void dgvCTPN_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvMAVT_HT = dgvCTPN.CurrentRow.Cells["dgvMAVT"].Value.ToString();
            if (e.ColumnIndex > 0 && dgvCTPN.Columns[e.ColumnIndex].Name == "dgvMAVT")
            {
                String sp_VattuDAT = "EXEC sp_VattuNhap '" + ((DataRowView)bdsPN[bdsPN.Position])["MasoDDH"].ToString() + "'";
                DataTable dt = new DataTable();
                dt = Program.ExecSqlDataTable(sp_VattuDAT);
                dgvMAVT.DataSource = dt;
                dgvMAVT.DisplayMember = "TENVT";
                dgvMAVT.ValueMember = "MAVT";
            }
            else { }
        }

        private void dgvCTPN_DataError(object sender, DataGridViewDataErrorEventArgs e) {}

        private void btnUseCTPN_Click(object sender, EventArgs e)
        {
            dgvCTPN.Enabled = true;
            dgvMAVT_HT = dgvCTPN.CurrentRow.Cells["dgvMAVT"].Value.ToString();
        }

        private void btnCloseCTPN_Click(object sender, EventArgs e)
        {
            dgvCTPN.Enabled = false;
        }

        
    }
}
