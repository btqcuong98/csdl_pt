﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.UserSkins;
using DevExpress.Skins;
using System.Data;
using System.Data.SqlClient;

namespace QLVT_DATHANG
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static SqlConnection conn = new SqlConnection();//-> cầu nối để kết nối vs DB, tồn tại suyên sốt chương trình
        public static String connstr; //kết nối site con
        public static SqlDataReader myReader;
        public static String servername = ""; //chưa tên phân mãnh (server)
        public static String username = ""; //cho biết ng nào <=> mã nhân viên
        public static String mlogin = ""; //giữ data trong textbox đăng nhập
        public static String password = "";

        public static String database = "QLVT_DATHANG";

        ///////
        public static String remotelogin = "HTKN";
        public static String remotepassword = "1234";
        //->link-> hỗ trợ kết nối từ xa
        //////

        public static String mloginDN = "";
        public static String passwordDN = "";
        public static String mGroup = ""; //chỗ nào sáng lên hay mờ đi
        public static String mHoten = ""; //in ở cuối form cho biết trạng thái-> ai đang làm việc
        public static int mChinhanh = 0;

        public static BindingSource bds_dspm = new BindingSource();  // giữ bdsPM khi đăng nhập

        public static int KetNoi()
        {

            if (Program.conn != null && Program.conn.State == ConnectionState.Open)
                Program.conn.Close();
           
            try
            {
                Program.connstr = "Data Source=" + Program.servername + ";Initial Catalog=" +
                      Program.database + ";User ID=" +
                      Program.mlogin + ";password=" + Program.password;
                Program.conn.ConnectionString = Program.connstr; // bắt đầu kết nối
                Program.conn.Open();
                return 1;
            }

            catch (Exception e)
            {
                MessageBox.Show("Lỗi kết nối cơ sở dữ liệu.\nBạn xem lại user name và password.\n " + e.Message, "", MessageBoxButtons.OK);
                return 0;
            }
        }
        //sp select view từ csdl, trả về 1 là reader/ 2 là table
        //data reader : ưu: truy vấn nhanh, nhiều dòng chỉ cho đi xuống, k đi ngược lại => dùng cho báo cáo || nhược: chỉ xem
        //data table -> cho phép sửa và cập nhật lại sql
        public static SqlDataReader ExecSqlDataReader(String strLenh)
        {
            SqlDataReader myreader;
            SqlCommand sqlcmd = new SqlCommand(strLenh, Program.conn); // gọi sp hoặc chạy truy vấn, chuỗi lệnh muốn chạy và chạy trên kết nối nào
            sqlcmd.CommandType = CommandType.Text;
            if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
            try
            {
                myreader = sqlcmd.ExecuteReader(); return myreader;

            }
            catch (SqlException ex)
            {
                Program.conn.Close();
                //MessageBox.Show(ex.Message);
                return null;
            }
        }
        public static DataTable ExecSqlDataTable(String cmd)
        {
            DataTable dt = new DataTable();
            if (Program.conn.State == ConnectionState.Closed) Program.conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd, conn);
            da.Fill(dt);
            conn.Close();
            return dt;
        }
        
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            BonusSkins.Register();
            //SkinManager.EnableFormSkins();
            Application.Run(new frmDangNhap());
            //Application.Run(new frmPhieuNhap());
        }
    }
}
