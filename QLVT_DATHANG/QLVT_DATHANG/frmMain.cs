﻿using QLVT_DATHANG.BAOCAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLVT_DATHANG
{
    public partial class frmMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmMain()
        {
            InitializeComponent();
            loadFormNV();
            loadStatus();
        }

        private void loadStatus()
        {
            ssMANV.Text = "Mã nhân viên: "+Program.username+" ||";
            ssHOTEN.Text = "Họ tên: " + Program.mHoten + " ||";
            ssNHOM.Text = "Nhóm quyền: " + Program.mGroup;
        }

        private Form CheckExists(Type ftype)
        {
            foreach (Form f in this.MdiChildren)
                if (f.GetType() == ftype)
                    return f;
            return null;
        }

        //MARK : -QUẢN LÝ

        //loadform
        private void loadFormNV()
        {
            Form frm = this.CheckExists(typeof(frmNhanVien));
            if (frm != null)
            {
                frm.Close();
            }
            frmNhanVien f = new frmNhanVien();
            f.MdiParent = this;
            f.Show();
        }

        private void loadFormKho()
        {
            Form frm = this.CheckExists(typeof(frmKho));
            if (frm != null)
            {
                frm.Close();
            }
            frmKho f = new frmKho();
            f.MdiParent = this;
            f.Show();

        }

        private void loadVatTu()
        {
            Form frm = this.CheckExists(typeof(frmVatTu));
            if (frm != null)
            {
                frm.Close();
            }
            frmVatTu f = new frmVatTu();
            f.MdiParent = this;
            f.Show();

        }

        private void TongHopNhapXuatTheoThang()
        {
            Form frm = this.CheckExists(typeof(Xfrm_TongHopNhapXuatTheoThang));
            if (frm != null)
            {
                frm.Close();
            }
            Xfrm_TongHopNhapXuatTheoThang f = new Xfrm_TongHopNhapXuatTheoThang();
            f.MdiParent = this;
            f.Show();

        }
        //action

        private void btnNhanVien_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadFormNV();
        }

        private void btnKho_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadFormKho();
        }

        private void btnVT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadVatTu();
        }

        //MARK : -NGHIỆP VỤ

        //loadform
        private void loadDDH()
        {
            Form frm = this.CheckExists(typeof(frmDatHang));
            if (frm != null)
            {
                frm.Close();
            }
            frmDatHang f = new frmDatHang();
            f.MdiParent = this;
            f.Show();

        }

        private void loadPhieuNhap()
        {
            Form frm = this.CheckExists(typeof(frmPhieuNhap));
            if (frm != null)
            {
                frm.Close();
            }
            frmPhieuNhap f = new frmPhieuNhap();
            f.MdiParent = this;
            f.Show();

        }

        private void loadPhieuXuat()
        {
            Form frm = this.CheckExists(typeof(frmPhieuXuat));
            if (frm != null)
            {
                frm.Close();
            }
            frmPhieuXuat f = new frmPhieuXuat();
            f.MdiParent = this;
            f.Show();

        }

        //action

        private void btnDH_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadDDH();
        }

        private void btnPN_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadPhieuNhap();
        }

        private void btnPX_ItemClick_1(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            loadPhieuXuat();
        }



        //MARK : - BÁO CÁO

        //loadform
        private void DSNV()
        {
            Form frm = this.CheckExists(typeof(Xfrm_DanhSachNhanVienTangDanTheoTenHo));
            if (frm != null)
            {
                frm.Close();
            }
            Xfrm_DanhSachNhanVienTangDanTheoTenHo f = new Xfrm_DanhSachNhanVienTangDanTheoTenHo();
            f.MdiParent = this;
            f.Show();

        }

        private void DSVT()
        {
            Form frm = this.CheckExists(typeof(Xfrm_VatTuTangDanTheoTen));
            if (frm != null)
            {
                frm.Close();
            }
            Xfrm_VatTuTangDanTheoTen f = new Xfrm_VatTuTangDanTheoTen();
            f.MdiParent = this;
            f.Show();

        }

        private void NhanVienLapPhieuTheoThang()
        {
            Form frm = this.CheckExists(typeof(Xfrm_NhanVienLapPhieuTheoThang));
            if (frm != null)
            {
                frm.Close();
            }
            Xfrm_NhanVienLapPhieuTheoThang f = new Xfrm_NhanVienLapPhieuTheoThang();
            f.MdiParent = this;
            f.Show();

        }
        //action
        private void btnDSNV_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DSNV();
        }

        private void btnDSVT_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DSVT();
        }

        private void btnDSNX_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
        }

        private void btnDSDDH_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void btnDSNVHD_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NhanVienLapPhieuTheoThang();
        }

        private void btnTHNX_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            TongHopNhapXuatTheoThang();
        }
    }
}
