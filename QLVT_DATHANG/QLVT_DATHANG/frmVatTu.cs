﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG
{
    public partial class frmVatTu : Form
    {
        Boolean flag = false;
        public frmVatTu()
        {
            InitializeComponent();
        }

        private void vattuBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.bdsVT.EndEdit();
            this.tableAdapterManager.UpdateAll(this.DS);

        }

        private void frmVatTu_Load(object sender, EventArgs e)
        {
            DS.EnforceConstraints = false;

            this.vattuTableAdapter.Connection.ConnectionString = Program.connstr;
            this.vattuTableAdapter.Fill(this.DS.Vattu);
            
            this.ctddhTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctddhTableAdapter.Fill(this.DS.CTDDH);
            
            this.ctpnTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctpnTableAdapter.Fill(this.DS.CTPN);
            
            this.ctpxTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctpxTableAdapter.Fill(this.DS.CTPX);

            groupBoxVT.Enabled = false;
            if (Program.mGroup == "CONGTY")
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = false;

            }
            else
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = true;

            }
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            flag = true;
            gcVT.Enabled = false;
            
            txtMaVT.Enabled = true;
            groupBoxVT.Enabled = true;
            bdsVT.AddNew();

            btnThem.Enabled = btnSua.Enabled = btnReload.Enabled = btnXoa.Enabled = false;
            btnUndo.Enabled = btnThoat.Enabled = true;

            txtSLT.Enabled = false;
            txtSLT.Text = "0";
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            flag = false;
            txtMaVT.Enabled = false;
            txtSLT.Enabled = true;
            btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = false;
            btnGhi.Enabled = btnReload.Enabled = true;
            groupBoxVT.Enabled = true;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            groupBoxVT.Enabled = false;
            btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = false;

            DialogResult rs = MessageBox.Show("Bạn có chắc muốn xóa ?", "XÓA VẬT TƯ", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                if (bdsCTDDH.Count > 0 || bdsCTPN.Count > 0 || bdsCTPX.Count > 0)
                {
                    MessageBox.Show("Bạn không được xóa các vật tư đã nằm trong chi tiết phiếu!", "Thông báo", MessageBoxButtons.OK);

                    bdsVT.Position = 0;
                    groupBoxVT.Enabled = false;
                    btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = true;
                }
                else
                {
                    try
                    {
                        bdsVT.EndEdit();
                        bdsVT.RemoveCurrent();
                        this.vattuTableAdapter.Connection.ConnectionString = Program.connstr;
                        this.vattuTableAdapter.Update(this.DS.Vattu);

                        MessageBox.Show("Bạn đã xóa thành công", "", MessageBoxButtons.OK);
                        bdsVT.Position = 0;
                        groupBoxVT.Enabled = false;
                        btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = true;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Lỗi xóa vật tư\n" + "Lỗi: " + ex, "", MessageBoxButtons.OK);
                        groupBoxVT.Enabled = false;
                        btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = true;

                        this.vattuTableAdapter.Fill(this.DS.Vattu);
                        return;
                    }
                }
            }
            else
            {
                groupBoxVT.Enabled = false;
                btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = true;
            }
        }

        private int checkMaVTAndTenVT(Boolean chon)
        {
            String querySQL = "SELECT * FROM Vattu WHERE MAVT = '" + txtMaVT.Text.Trim() + "'";
            String messageErr = "Mã vật tư đã tồn tại";
            if (chon == true)
            {
                querySQL = "SELECT * FROM Vattu WHERE TENVT = N'" + txtTenVT.Text.Trim() + "'";
                messageErr = "Tên vật tư đã tồn tại";
            }
            Program.myReader = Program.ExecSqlDataReader(querySQL);

            if (Program.myReader.Read() == true)
            {
                MessageBox.Show(messageErr, "THÔNG BÁO", MessageBoxButtons.OK);
                txtMaVT.Focus();
                if (chon == true) txtTenVT.Focus();
                Program.myReader.Close();
                return 1;
            }
            Program.myReader.Close();
            return 0;
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //kiểm tra mã vật tư
            if(flag == true)
            {
                if (checkMaVTAndTenVT(false) == 1) return;
            }
            //kiểm tra tên vật tư
            if (flag == true)
            {
                if (checkMaVTAndTenVT(true) == 1) return;
            }
            else 
            {
                if (((DataRowView)bdsVT[bdsVT.Position])["TENVT"].ToString() != txtTenVT.Text.Trim())
                {
                    if (checkMaVTAndTenVT(true) == 1) return;                
                }
            }
            //kiểm tra rỗng và điều kiện input
            if (txtMaVT.Text.Trim() == "")
            {
                MessageBox.Show("Bạn chưa nhập mã vật tư!", "THÔNG BÁO", MessageBoxButtons.OK);
                txtMaVT.Focus();
                return;
            }
            if (txtTenVT.Text.Trim() == "")
            {
                MessageBox.Show("Bạn chưa nhập tên vật tư!", "THÔNG BÁO", MessageBoxButtons.OK);
                txtTenVT.Focus();
                return;
            }
            if (txtDVT.Text.Trim() == "")
            {
                MessageBox.Show("Bạn chưa nhập đơn vị tính!", "THÔNG BÁO", MessageBoxButtons.OK);
                txtDVT.Focus();
                return;
            }
            if (Int32.Parse(txtSLT.Text.Trim()) < 0)
            {
                MessageBox.Show("Số lượng tồn không được nhỏ hơn 0!", "THÔNG BÁO", MessageBoxButtons.OK);
                txtSLT.Focus();
                return;
            }
            try
            {

                bdsVT.EndEdit();
                bdsVT.ResetCurrentItem();
                this.vattuTableAdapter.Connection.ConnectionString = Program.connstr;
                this.vattuTableAdapter.Update(this.DS.Vattu);

                MessageBox.Show("Ghi thành công!", "THÔNG BÁO", MessageBoxButtons.OK);

                groupBoxVT.Enabled = false;
                btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = true;
                btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = true;
                gcVT.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi ghi vật tư\n " + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            groupBoxVT.Enabled = false;
            gcVT.Enabled = true;
            if (Program.mGroup == "CONGTY")
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = false;

            }
            else
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = true;

            }

            bdsVT.CancelEdit();
            bdsVT.EndEdit();
            this.vattuTableAdapter.Fill(DS.Vattu);
        }

        private void btnReload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = groupBoxVT.Enabled = false;
            this.vattuTableAdapter.Fill(this.DS.Vattu);

            MessageBox.Show("Tải lại dữ liệu thành công", "Thông báo", MessageBoxButtons.OK);


            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = true;
            gcVT.Enabled = true;
            groupBoxVT.Enabled = false;
            bdsVT.Position = 0;
        }

        private void btnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }
    }
}
