﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG
{
    public partial class frmPhieuXuat : Form
    {
        String maCN = "";
        Boolean isEdit = true;
        String dgvMAVT_HT = "";

        public frmPhieuXuat()
        {
            InitializeComponent();
        }

        private void phieuXuatBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.bdsPX.EndEdit();
            this.tableAdapterManager.UpdateAll(this.DS);

        }

        public void sp_VattuXuat_dgvCombobox()
        {
            String sp_VattuXUAT = "EXEC sp_VattuXuat '" + ((DataRowView)bdsPX[bdsPX.Position])["MAKHO"].ToString() + "'";
            DataTable dt = new DataTable();
            dt = Program.ExecSqlDataTable(sp_VattuXUAT);
            dgvMaVT.DataSource = dt;
            dgvMaVT.DisplayMember = "TENVT";
            dgvMaVT.ValueMember = "MAVT";
            
        }

        public void CTPX_dgvCombobox()
        {
           
            //HIỂN THỊ LẦN ĐÂU KHI CHƯA CLICK
            String vattuCTPX = "SELECT  MAVT FROM CTPX WHERE MAPX = '" + ((DataRowView)bdsPX[bdsPX.Position])["MAPX"].ToString() + "'";
            Program.myReader = Program.ExecSqlDataReader(vattuCTPX);
            if (Program.myReader == null)
            {
                return;
            }
            int rowCTPX = 0;
            while (Program.myReader.Read())
            {

                dgvCTPX.Rows[rowCTPX].Cells["dgvMaVT"].Value = Program.myReader.GetString(0);
                rowCTPX++;
            }
            Program.myReader.Close();
        }

        private void frmPhieuXuat_Load(object sender, EventArgs e)
        {
            
            DS.EnforceConstraints = false;

            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

            this.ctpxTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctpxTableAdapter.Fill(this.DS.CTPX);

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dsnvTableAdapter.Fill(this.DS.DSNV);

            //cmb rẽ site
            maCN = ((DataRowView)bdsKho[0])["MACN"].ToString();

            cmbTenCN.DataSource = Program.bds_dspm;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedIndex = Program.mChinhanh;

            //
            panelControl1.Enabled = false;
            dgvCTPX.Enabled = false;
            if (Program.mGroup == "CONGTY")
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = false;
            }
            else
            {
                cmbTenCN.Enabled = false;
            }
            
            //
            cmbHOTEN.SelectedValue = txbMaNV.Text;
            cmbTENKHO.SelectedValue = txbMaKHO.Text;

            //sub form
            sp_VattuXuat_dgvCombobox();
            CTPX_dgvCombobox();
        }

        private void cmbTenCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTenCN.SelectedValue.ToString() == "System.Data.DataRowView")
            {
                return;
            }
            Program.servername = cmbTenCN.SelectedValue.ToString();

            if (cmbTenCN.SelectedIndex != Program.mChinhanh)
            {

                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }

            if (Program.KetNoi() == 0)
            {
                MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
            }
            else
            {
                this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
                this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

                this.ctpxTableAdapter.Connection.ConnectionString = Program.connstr;
                this.ctpxTableAdapter.Fill(this.DS.CTPX);

                this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
                this.khoTableAdapter.Fill(this.DS.Kho);

                this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
                this.dsnvTableAdapter.Fill(this.DS.DSNV);

                maCN = ((DataRowView)bdsKho[0])["MACN"].ToString();
            }
        }

        private void cmbHOTEN_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txbMaNV.Text = cmbHOTEN.SelectedValue.ToString();
            }
            catch (Exception ex) { }
        }

        private void cmbTENKHO_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txbMaKHO.Text = cmbTENKHO.SelectedValue.ToString();
            }
            catch (Exception ex) { }
        }

        private void btnAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //
            isEdit = false;
            //
            txbMaPX.Enabled = true;
            //
            gcPX.Enabled = false;
            //
            panelControl1.Enabled = true;

            bdsPX.AddNew();

            //
            cmbHOTEN.Enabled = false;
            cmbHOTEN.SelectedValue = Program.username;
            txbMaNV.Text = Program.username;

            //
            btnAdd.Enabled = false;
            btnDelete.Enabled = btnEdit.Enabled = btnReLoad.Enabled = btnExit.Enabled = false;
            btnSave.Enabled = btnUndo.Enabled = true;
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            isEdit = true;
            cmbHOTEN.Enabled = true;
            txbMaPX.Enabled = false;
            panelControl1.Enabled = true;
            btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = false;
            btnSave.Enabled = btnUndo.Enabled = true;
        }

        private int sp_TimPX()
        {
            String sp_TimPX = "EXEC sp_TimPX '" + txbMaPX.Text.Trim() + "'";
            Program.myReader = Program.ExecSqlDataReader(sp_TimPX);
            if (Program.myReader != null)
            {
                MessageBox.Show("Mã phiếu nhập đã tồn tại", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaPX.Focus();
                Program.myReader.Close();
                return 1;
            }
            Program.myReader.Close();
            return 0;
        }

        private void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (txbMaPX.Text.Trim() == "")
            {
                MessageBox.Show("Mã phiếu xuất không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaPX.Focus();
                return;
            }

            //ktra mã trùng
            if (!isEdit)
            {
                if (sp_TimPX() == 1)
                {
                    return;
                }
            }

            if (dptNGAY.Text.Trim() == "")
            {
                MessageBox.Show("Ngày nhập không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                dptNGAY.Focus();
                return;
            }

            if (txbHOTENKH.Text.Trim() == "")
            {
                MessageBox.Show("Mã đơn đặt không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbHOTENKH.Focus();
                return;
            }

            if (txbMaNV.Text.Trim() == "")
            {
                MessageBox.Show("Nhân viên lập phiếu không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaNV.Focus();
                return;
            }

            if (txbMaKHO.Text.Trim() == "")
            {
                MessageBox.Show("Mã kho không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaKHO.Focus();
                return;
            }

            try
            {
                bdsPX.EndEdit();
                bdsPX.ResetCurrentItem();
                this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
                this.phieuXuatTableAdapter.Update(this.DS.PhieuXuat);

                MessageBox.Show("Ghi thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                panelControl1.Enabled = false;
                btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = true;
                btnSave.Enabled = btnUndo.Enabled = true;
                gcPX.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi xuất hàng\n " + ex.Message, "", MessageBoxButtons.OK);
                panelControl1.Enabled = false;
                btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = true;
                btnSave.Enabled = btnUndo.Enabled = true;
                gcPX.Enabled = true;
                return;
            }
        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            panelControl1.Enabled = false;
            btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = false;

            DialogResult rs = MessageBox.Show("Bạn có muốn xóa phiếu xuất này không ?", "THÔNG BÁO", MessageBoxButtons.YesNo);

            if (DialogResult.Yes == rs)
            {
                if (bdsCTPX.Count > 0)
                {
                    MessageBox.Show("Phiếu nhập này không thể xóa !", "THÔNG BÁO", MessageBoxButtons.OK);
                    bdsPX.Position = 0;
                    panelControl1.Enabled = false;
                    btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                }
                else
                {
                    try
                    {
                        bdsPX.EndEdit();
                        bdsPX.RemoveCurrent();
                        this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
                        this.phieuXuatTableAdapter.Update(this.DS.PhieuXuat);


                        MessageBox.Show("Xóa thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                        bdsPX.Position = 0;
                        panelControl1.Enabled = false;
                        btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Xóa thất bại Lỗi: " + ex.Message, "THÔNG BÁO", MessageBoxButtons.OK);
                    }
                }
            }
            else
            {
                panelControl1.Enabled = true;
                btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
            }
        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            panelControl1.Enabled = false;
            if (Program.mGroup == "CONGTY")
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = false;
                cmbTenCN.Enabled = btnReLoad.Enabled = true;
                gcPX.Enabled = true;
            }
            else
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = true;
                cmbTenCN.Enabled = false;
                gcPX.Enabled = true;
            }

            bdsPX.CancelEdit();
            bdsPX.EndEdit();

            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

            this.ctpxTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctpxTableAdapter.Fill(this.DS.CTPX);

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dsnvTableAdapter.Fill(this.DS.DSNV);
        }

        private void btnReLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnExit.Enabled = panelControl1.Enabled = false;

            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

            this.ctpxTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctpxTableAdapter.Fill(this.DS.CTPX);

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dsnvTableAdapter.Fill(this.DS.DSNV);

            MessageBox.Show("Tải lại dữ liệu thành công", "Thông báo", MessageBoxButtons.OK);


            btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
            panelControl1.Enabled = false;
            bdsPX.Position = 0;
            gcPX.Enabled = true;
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }


        /*ContextMenuStrip*/

        private int soLuong()
        {
            String soLuong = "EXEC sp_SOLUONGTON '"+dgvCTPX.CurrentRow.Cells["dgvMaVT"].Value.ToString()+"', '"
                                                                     + ((DataRowView)bdsPX[bdsPX.Position])["MAKHO"].ToString()+"'";
         
            Program.myReader = Program.ExecSqlDataReader(soLuong);
            if (Program.myReader == null)
            {
                MessageBox.Show("Vui lòng kiểm tra chi tiết xuất hàng !", "THÔNG BÁO", MessageBoxButtons.OK);
                return 0;
            }

            Program.myReader.Read();
            int sl = Int32.Parse(Program.myReader.GetInt32(4).ToString());
            Program.myReader.Close();
            Program.conn.Close();
            return sl;
        }

        private int checkCTPX()
        {
            String checkCTPX = "SELECT MAPX, MAVT FROM CTPX WHERE MAVT='" + dgvCTPX.CurrentRow.Cells["dgvMAVT"].Value.ToString()
                                    + "' AND MAPX = '" + ((DataRowView)bdsPX[bdsPX.Position])["MAPX"].ToString() + "'";
            Program.myReader = Program.ExecSqlDataReader(checkCTPX);
            if (Program.myReader.Read() == false)
            {
                Program.myReader.Close();
                return 1;
            }
            Program.myReader.Close();
            MessageBox.Show("Chi tiết đơn hàng này đã tồn tại !", "THÔNG BÁO", MessageBoxButtons.OK);
            return 0;
        }

        private void btnSaveCTPX_Click(object sender, EventArgs e)
        {
            if (dgvCTPX.CurrentRow.Cells["dgvMaVT"].Value.ToString().Trim() == "")
            {
                MessageBox.Show("Mã vật tư không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            if (dgvCTPX.CurrentRow.Cells["dgvSOLUONG"].Value.ToString().Trim() == "")
            {
                MessageBox.Show("Số lượng không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            if (Int32.Parse(dgvCTPX.CurrentRow.Cells["dgvSOLUONG"].Value.ToString()) > soLuong())
            {
                MessageBox.Show("Số lượng không hợp lệ \nSố lượng hiện tại trong kho là :" + soLuong(), "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            if (dgvCTPX.CurrentRow.Cells["dgvDONGIA"].Value.ToString().Trim() == "")
            {
                MessageBox.Show("Đơn giá không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            if (dgvCTPX.CurrentRow.Cells["dgvMAVT"].Value.ToString() != dgvMAVT_HT && checkCTPX() == 0)
            {
                return;
            }

            try
            {

                bdsCTPX.EndEdit();
                bdsCTPX.ResetCurrentItem();
                this.ctpxTableAdapter.Connection.ConnectionString = Program.connstr;
                this.ctpxTableAdapter.Update(this.DS.CTPX);

                MessageBox.Show("Ghi thành công !", "THÔNG BÁO", MessageBoxButtons.OK);
                this.phieuXuatTableAdapter.Fill(DS.PhieuXuat);

               // bdsPX.Position = 0;
                dgvCTPX.Enabled = false;
                sp_VattuXuat_dgvCombobox();
                CTPX_dgvCombobox();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi xuất hàng\n " + ex.Message, "", MessageBoxButtons.OK);
                dgvCTPX.Enabled = false;
                return;
            }
        }

        private void btnDeleteCTPX_Click(object sender, EventArgs e)
        {
            //panelControl1.Enabled = false;
            //btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = false;

            DialogResult rs = MessageBox.Show("Bạn có muốn xóa phiếu xuất này không ?", "THÔNG BÁO", MessageBoxButtons.YesNo);

            if (DialogResult.Yes == rs)
            {
                try
                {
                    bdsCTPX.EndEdit();
                    bdsCTPX.RemoveCurrent();
                    this.ctpxTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.ctpxTableAdapter.Update(this.DS.CTPX);
                    this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

                    sp_VattuXuat_dgvCombobox();
                    CTPX_dgvCombobox();

                    MessageBox.Show("Xóa thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                    //bdsPX.Position = 0;
                    //panelControl1.Enabled = false;
                    //btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                    dgvCTPX.Enabled = false;
                    
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Xóa thất bại Lỗi: " + ex.Message, "THÔNG BÁO", MessageBoxButtons.OK);
                    //bdsPX.Position = 0;
                    dgvCTPX.Enabled = false;
                    return;
                }
            }
            else
            {
                //bdsPX.Position = 0;
                dgvCTPX.Enabled = false;
                //panelControl1.Enabled = true;
                //btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
            }
        }

        private void gcPX_Click(object sender, EventArgs e)
        {
            sp_VattuXuat_dgvCombobox();
            CTPX_dgvCombobox();
        }

        private void dgvCTPX_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            dgvMAVT_HT = dgvCTPX.CurrentRow.Cells["dgvMAVT"].Value.ToString();
            if (e.ColumnIndex > 0 && dgvCTPX.Columns[e.ColumnIndex].Name == "dgvMaVT")
            {
                String sp_vattuXuat = "EXEC sp_vattuXuat '" + ((DataRowView)bdsPX[bdsPX.Position])["MAKHO"].ToString() + "'";
                DataTable dt = new DataTable();
                dt = Program.ExecSqlDataTable(sp_vattuXuat);
                dgvMaVT.DataSource = dt;
                dgvMaVT.DisplayMember = "TENVT";
                dgvMaVT.ValueMember = "MAVT";

                
            }
        }

        private void dgvCTPX_DataError(object sender, DataGridViewDataErrorEventArgs e) { }

        private void btnUseCTPX_Click(object sender, EventArgs e)
        {
            dgvCTPX.Enabled = true;
            dgvMAVT_HT = dgvCTPX.CurrentRow.Cells["dgvMAVT"].Value.ToString();
        }

        private void btnClockCTPX_Click(object sender, EventArgs e)
        {
            dgvCTPX.Enabled = false;
        }

    }
}
