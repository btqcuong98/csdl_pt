﻿using DevExpress.XtraReports.UI;
using QLVT_DATHANG.BAOCAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG.BAOCAO
{
    public partial class Xfrm_DanhSachNhanVienTangDanTheoTenHo : Form
    {
        public Xfrm_DanhSachNhanVienTangDanTheoTenHo()
        {
            InitializeComponent();
        }

        private void Xfrm_DanhSachNhanVienTangDanTheoTenHo_Load(object sender, EventArgs e)
        {
            this.sp_InDSNVTableAdapter.Connection.ConnectionString = Program.connstr;
            this.sp_InDSNVTableAdapter.Fill(this.DS.sp_InDSNV);

            cmbTenCN.DataSource = Program.bds_dspm;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedIndex = Program.mChinhanh;

            if (Program.mGroup == "CONGTY")
            {
                cmbTenCN.Enabled = true;
            }
            else
            {
                cmbTenCN.Enabled = false;
            }
        }

        private void cmbTenCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTenCN.SelectedValue.ToString() != "System.Data.DataRowView")
            {
                Program.servername = cmbTenCN.SelectedValue.ToString();
            }

            if (cmbTenCN.SelectedIndex != Program.mChinhanh)
            {

                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }

            if (Program.KetNoi() == 0)
            {
                MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
            }
            else
            {
                this.sp_InDSNVTableAdapter.Connection.ConnectionString = Program.connstr;
                this.sp_InDSNVTableAdapter.Fill(this.DS.sp_InDSNV);
            }
        }

        private void btnPreView_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Xrpt_DanhSachNhanVienTangDanTheoTenHo rpt = new Xrpt_DanhSachNhanVienTangDanTheoTenHo();
            rpt.xrlblCN.Text =  cmbTenCN.Text;
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowRibbonPreviewDialog();
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }
    }
}
