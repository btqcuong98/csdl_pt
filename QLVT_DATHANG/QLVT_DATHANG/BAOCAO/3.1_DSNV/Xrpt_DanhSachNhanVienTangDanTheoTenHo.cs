﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLVT_DATHANG.BAOCAO
{
    public partial class Xrpt_DanhSachNhanVienTangDanTheoTenHo : DevExpress.XtraReports.UI.XtraReport
    {
        public Xrpt_DanhSachNhanVienTangDanTheoTenHo()
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_InDSNVTableAdapter1.Connection.ConnectionString = Program.connstr;
            this.sp_InDSNVTableAdapter1.Fill(ds1.sp_InDSNV);
        }

    }
}
