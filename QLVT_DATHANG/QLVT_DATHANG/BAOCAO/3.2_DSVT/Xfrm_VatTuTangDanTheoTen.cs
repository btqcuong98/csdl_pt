﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;

namespace QLVT_DATHANG.BAOCAO
{
    public partial class Xfrm_VatTuTangDanTheoTen : Form
    {

        public Xfrm_VatTuTangDanTheoTen()
        {
            InitializeComponent();

        }


        private void Xfrm_VatTuTangDanTheoTen_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dS.sp_InDSVT' table. You can move, or remove it, as needed.
            this.sp_InDSVTTableAdapter.Fill(this.DS.sp_InDSVT);

        }

        private void btnPreview_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Xrpt_VatTuTangDanTheoTen rpt = new Xrpt_VatTuTangDanTheoTen();
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowRibbonPreviewDialog();
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close();
        }
    }
}
