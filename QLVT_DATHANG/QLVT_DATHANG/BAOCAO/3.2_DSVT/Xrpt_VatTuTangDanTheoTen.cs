﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLVT_DATHANG.BAOCAO
{
    public partial class Xrpt_VatTuTangDanTheoTen : DevExpress.XtraReports.UI.XtraReport
    {
        public Xrpt_VatTuTangDanTheoTen()
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_InDSVTTableAdapter1.Fill(this.ds1.sp_InDSVT);
        }
    }
}
