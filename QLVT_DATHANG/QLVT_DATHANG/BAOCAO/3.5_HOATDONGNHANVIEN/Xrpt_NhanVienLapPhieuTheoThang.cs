﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLVT_DATHANG.BAOCAO
{
    public partial class Xrpt_NhanVienLapPhieuTheoThang : DevExpress.XtraReports.UI.XtraReport
    {
        public Xrpt_NhanVienLapPhieuTheoThang(int manv, DateTime from, DateTime to)
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_NhanVienLapPhieuTheoThangTableAdapter1.Connection.ConnectionString = Program.connstr;
            this.sp_NhanVienLapPhieuTheoThangTableAdapter1.Fill(ds1.sp_NhanVienLapPhieuTheoThang, manv, from, to);
        }

    }
}
