﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG.BAOCAO
{
    public partial class Xfrm_NhanVienLapPhieuTheoThang : Form
    {
        public Xfrm_NhanVienLapPhieuTheoThang()
        {
            InitializeComponent();
        }

        private void Xfrm_NhanVienLapPhieuTheoThang_Load(object sender, EventArgs e)
        {
            DS.EnforceConstraints = false;
            this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dsnvTableAdapter.Fill(this.DS.DSNV);

            cmbTenCN.DataSource = Program.bds_dspm;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedIndex = Program.mChinhanh;

            if (Program.mGroup == "CONGTY")
            {
                cmbTenCN.Enabled = true;
            }
            else
            {
                cmbTenCN.Enabled = false;
            }

        }

        private void btnPreView_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(txbMaNV.Text.Trim() == "")
            {
                MessageBox.Show("Họ Tên Nhân Viên không được để trống", "Thông báo", MessageBoxButtons.OK);
                txbMaNV.Focus();
                return;
            }

            if (dtpFROM.Text.Trim() == "")
            {
                MessageBox.Show("Ngày bắt đầu không được để trống", "Thông báo", MessageBoxButtons.OK);
                dtpFROM.Focus();
                return;
            }

            if (dtpTO.Text.Trim() == "")
            {
                MessageBox.Show("Ngày kết thúc không được để trống", "Thông báo", MessageBoxButtons.OK);
                dtpTO.Focus();
                return;
            }

            DS.EnforceConstraints = false;
            this.sp_NhanVienLapPhieuTheoThangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.sp_NhanVienLapPhieuTheoThangTableAdapter.Fill(DS.sp_NhanVienLapPhieuTheoThang, Int32.Parse(txbMaNV.Text), Convert.ToDateTime(dtpFROM.Text), Convert.ToDateTime(dtpTO.Text));

            Xrpt_NhanVienLapPhieuTheoThang rpt = new Xrpt_NhanVienLapPhieuTheoThang(Int32.Parse(txbMaNV.Text), Convert.ToDateTime(dtpFROM.Text), Convert.ToDateTime(dtpTO.Text));
            rpt.lblMaNV.Text = txbMaNV.Text;
            rpt.lblName.Text = cmbHOTEN.Text;
            foreach (DataRow row in this.DS.DSNV.Rows)
            {
                rpt.lblDate.Text = String.Format("{0:dd/MM/yyyy}", row["NGAYSINH"].ToString());
                rpt.lblSalary.Text = row["LUONG"].ToString();
                rpt.lblAddress.Text = row["DIACHI"].ToString();
            }
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowRibbonPreviewDialog();
 
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void cmbTenCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTenCN.SelectedValue.ToString() == "System.Data.DataRowView")
            {
                return;
                
            }
            Program.servername = cmbTenCN.SelectedValue.ToString();

            if (cmbTenCN.SelectedIndex != Program.mChinhanh)
            {

                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }

            if (Program.KetNoi() == 0)
            {
                MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
            }
            else
            {
                if (txbMaNV.Text.Trim() == "")
                {
                    return;
                }

                if (dtpFROM.Text.Trim() == "")
                {
                    return;
                }

                if (dtpTO.Text.Trim() == "")
                {
                    return;
                }
                this.sp_NhanVienLapPhieuTheoThangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.sp_NhanVienLapPhieuTheoThangTableAdapter.Fill(DS.sp_NhanVienLapPhieuTheoThang, Int32.Parse(txbMaNV.Text), Convert.ToDateTime(dtpFROM.Text), Convert.ToDateTime(dtpTO.Text));

                this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
                this.dsnvTableAdapter.Fill(this.DS.DSNV);
            }
        }

        private void cmbNAME_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                txbMaNV.Text = cmbHOTEN.SelectedValue.ToString();
            }
            catch (Exception ex) { }
        }

    }
}
