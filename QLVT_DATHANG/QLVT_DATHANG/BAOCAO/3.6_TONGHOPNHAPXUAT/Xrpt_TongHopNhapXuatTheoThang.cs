﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace QLVT_DATHANG.BAOCAO
{
    public partial class Xrpt_TongHopNhapXuatTheoThang : DevExpress.XtraReports.UI.XtraReport
    {
        public Xrpt_TongHopNhapXuatTheoThang(DateTime from, DateTime to)
        {
            InitializeComponent();
            ds1.EnforceConstraints = false;
            this.sp_TongHopNhapXuatTheoThangTableAdapter1.Connection.ConnectionString = Program.connstr;
            this.sp_TongHopNhapXuatTheoThangTableAdapter1.Fill(ds1.sp_TongHopNhapXuatTheoThang, from, to);
        }

    }
}
