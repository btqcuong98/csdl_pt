﻿using DevExpress.XtraReports.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG.BAOCAO
{
    public partial class Xfrm_TongHopNhapXuatTheoThang : Form
    {
        public Xfrm_TongHopNhapXuatTheoThang()
        {
            InitializeComponent();
        }

        private void Xfrm_TongHopNhapXuatTheoThang_Load(object sender, EventArgs e)
        {
            
            cmbTenCN.DataSource = Program.bds_dspm;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedIndex = Program.mChinhanh;

            if (Program.mGroup == "CONGTY")
            {
                cmbTenCN.Enabled = true;
            }
            else
            {
                cmbTenCN.Enabled = false;
            }
        }

        private void cmbTenCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTenCN.SelectedValue.ToString() == "System.Data.DataRowView")
            {
                return;

            }
            Program.servername = cmbTenCN.SelectedValue.ToString();

            if (cmbTenCN.SelectedIndex != Program.mChinhanh)
            {

                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }

            if (Program.KetNoi() == 0)
            {
                MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
            }
            else
            {
                if (dtpFROM.Text.Trim() == "")
                {
                    return;
                }

                if (dtpTO.Text.Trim() == "")
                {
                    return;
                }
                this.sp_TongHopNhapXuatTheoThangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.sp_TongHopNhapXuatTheoThangTableAdapter.Fill(this.DS.sp_TongHopNhapXuatTheoThang, Convert.ToDateTime(dtpFROM.Text), Convert.ToDateTime(dtpTO.Text));
            }
        }

        private void btnPreView_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (dtpFROM.Text.Trim() == "")
            {
                return;
            }

            if (dtpTO.Text.Trim() == "")
            {
                return;
            }

            this.sp_TongHopNhapXuatTheoThangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.sp_TongHopNhapXuatTheoThangTableAdapter.Fill(this.DS.sp_TongHopNhapXuatTheoThang, Convert.ToDateTime(dtpFROM.Text), Convert.ToDateTime(dtpTO.Text));

            Xrpt_TongHopNhapXuatTheoThang rpt = new Xrpt_TongHopNhapXuatTheoThang(Convert.ToDateTime(dtpFROM.Text), Convert.ToDateTime(dtpTO.Text));
            rpt.lblFROM.Text = dtpFROM.Text;
            rpt.lblTO.Text = dtpTO.Text;
            ReportPrintTool print = new ReportPrintTool(rpt);
            print.ShowRibbonPreviewDialog();
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

    }
}
