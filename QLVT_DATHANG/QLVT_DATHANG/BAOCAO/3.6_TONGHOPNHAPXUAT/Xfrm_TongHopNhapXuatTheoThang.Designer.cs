﻿namespace QLVT_DATHANG.BAOCAO
{
    partial class Xfrm_TongHopNhapXuatTheoThang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Xfrm_TongHopNhapXuatTheoThang));
            this.cmbTenCN = new System.Windows.Forms.ComboBox();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnPreView = new DevExpress.XtraBars.BarButtonItem();
            this.btnExit = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dtpTO = new System.Windows.Forms.DateTimePicker();
            this.dtpFROM = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DS = new QLVT_DATHANG.DS();
            this.bds_sp_TongHopNhapXuatTheoThang = new System.Windows.Forms.BindingSource(this.components);
            this.sp_TongHopNhapXuatTheoThangTableAdapter = new QLVT_DATHANG.DSTableAdapters.sp_TongHopNhapXuatTheoThangTableAdapter();
            this.tableAdapterManager = new QLVT_DATHANG.DSTableAdapters.TableAdapterManager();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNGAY = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTYLENHAP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colXUAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTYLEXUAT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.sp_TongHopNhapXuatTheoThangGridControl = new DevExpress.XtraGrid.GridControl();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_sp_TongHopNhapXuatTheoThang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_TongHopNhapXuatTheoThangGridControl)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbTenCN
            // 
            this.cmbTenCN.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTenCN.FormattingEnabled = true;
            this.cmbTenCN.Location = new System.Drawing.Point(191, 42);
            this.cmbTenCN.Name = "cmbTenCN";
            this.cmbTenCN.Size = new System.Drawing.Size(268, 27);
            this.cmbTenCN.TabIndex = 4;
            this.cmbTenCN.SelectedIndexChanged += new System.EventHandler(this.cmbTenCN_SelectedIndexChanged);
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnPreView,
            this.btnExit});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 1;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Tools";
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnPreView, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btnExit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btnPreView
            // 
            this.btnPreView.Caption = "IN DANH SÁCH";
            this.btnPreView.Id = 0;
            this.btnPreView.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnPreView.ImageOptions.Image")));
            this.btnPreView.Name = "btnPreView";
            this.btnPreView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPreView_ItemClick);
            // 
            // btnExit
            // 
            this.btnExit.Caption = "THOÁT";
            this.btnExit.Id = 1;
            this.btnExit.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExit.ImageOptions.Image")));
            this.btnExit.Name = "btnExit";
            this.btnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExit_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Manager = this.barManager1;
            this.barDockControlTop.Size = new System.Drawing.Size(1200, 69);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 635);
            this.barDockControlBottom.Manager = this.barManager1;
            this.barDockControlBottom.Size = new System.Drawing.Size(1200, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 69);
            this.barDockControlLeft.Manager = this.barManager1;
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 566);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1200, 69);
            this.barDockControlRight.Manager = this.barManager1;
            this.barDockControlRight.Size = new System.Drawing.Size(0, 566);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.label1.Location = new System.Drawing.Point(88, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 19);
            this.label1.TabIndex = 5;
            this.label1.Text = "CHI NHÁNH";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dtpTO);
            this.groupBox2.Controls.Add(this.dtpFROM);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1200, 47);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // dtpTO
            // 
            this.dtpTO.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTO.Location = new System.Drawing.Point(735, 14);
            this.dtpTO.Name = "dtpTO";
            this.dtpTO.Size = new System.Drawing.Size(163, 26);
            this.dtpTO.TabIndex = 14;
            this.dtpTO.Value = new System.DateTime(2019, 12, 23, 0, 0, 0, 0);
            // 
            // dtpFROM
            // 
            this.dtpFROM.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFROM.Location = new System.Drawing.Point(283, 15);
            this.dtpFROM.Name = "dtpFROM";
            this.dtpFROM.Size = new System.Drawing.Size(163, 26);
            this.dtpFROM.TabIndex = 13;
            this.dtpFROM.Value = new System.DateTime(2019, 12, 23, 0, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.HighlightText;
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(684, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 19);
            this.label3.TabIndex = 12;
            this.label3.Text = "ĐẾN ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.HighlightText;
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(242, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 19);
            this.label2.TabIndex = 11;
            this.label2.Text = "TỪ ";
            // 
            // DS
            // 
            this.DS.DataSetName = "DS";
            this.DS.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bds_sp_TongHopNhapXuatTheoThang
            // 
            this.bds_sp_TongHopNhapXuatTheoThang.DataMember = "sp_TongHopNhapXuatTheoThang";
            this.bds_sp_TongHopNhapXuatTheoThang.DataSource = this.DS;
            // 
            // sp_TongHopNhapXuatTheoThangTableAdapter
            // 
            this.sp_TongHopNhapXuatTheoThangTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ChiNhanhTableAdapter = null;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.CTDDHTableAdapter = null;
            this.tableAdapterManager.CTPNTableAdapter = null;
            this.tableAdapterManager.CTPXTableAdapter = null;
            this.tableAdapterManager.DatHangTableAdapter = null;
            this.tableAdapterManager.KhoTableAdapter = null;
            this.tableAdapterManager.NhanVienTableAdapter = null;
            this.tableAdapterManager.PhieuNhapTableAdapter = null;
            this.tableAdapterManager.PhieuXuatTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = QLVT_DATHANG.DSTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.VattuTableAdapter = null;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNGAY,
            this.colNHAP,
            this.colTYLENHAP,
            this.colXUAT,
            this.colTYLEXUAT});
            this.gridView1.GridControl = this.sp_TongHopNhapXuatTheoThangGridControl;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            // 
            // colNGAY
            // 
            this.colNGAY.FieldName = "NGAY";
            this.colNGAY.Name = "colNGAY";
            this.colNGAY.Visible = true;
            this.colNGAY.VisibleIndex = 0;
            // 
            // colNHAP
            // 
            this.colNHAP.DisplayFormat.FormatString = "#,##0.0";
            this.colNHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNHAP.FieldName = "NHAP";
            this.colNHAP.Name = "colNHAP";
            this.colNHAP.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(),
            new DevExpress.XtraGrid.GridColumnSummaryItem(),
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "NHAP", "{0:#,##.0}"),
            new DevExpress.XtraGrid.GridColumnSummaryItem()});
            this.colNHAP.Visible = true;
            this.colNHAP.VisibleIndex = 1;
            // 
            // colTYLENHAP
            // 
            this.colTYLENHAP.DisplayFormat.FormatString = "0.00%";
            this.colTYLENHAP.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTYLENHAP.FieldName = "TYLENHAP";
            this.colTYLENHAP.Name = "colTYLENHAP";
            this.colTYLENHAP.Visible = true;
            this.colTYLENHAP.VisibleIndex = 2;
            // 
            // colXUAT
            // 
            this.colXUAT.DisplayFormat.FormatString = "#,##0.0";
            this.colXUAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colXUAT.FieldName = "XUAT";
            this.colXUAT.Name = "colXUAT";
            this.colXUAT.Visible = true;
            this.colXUAT.VisibleIndex = 3;
            // 
            // colTYLEXUAT
            // 
            this.colTYLEXUAT.DisplayFormat.FormatString = "0.00%";
            this.colTYLEXUAT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTYLEXUAT.FieldName = "TYLEXUAT";
            this.colTYLEXUAT.Name = "colTYLEXUAT";
            this.colTYLEXUAT.Visible = true;
            this.colTYLEXUAT.VisibleIndex = 4;
            // 
            // sp_TongHopNhapXuatTheoThangGridControl
            // 
            this.sp_TongHopNhapXuatTheoThangGridControl.DataSource = this.bds_sp_TongHopNhapXuatTheoThang;
            this.sp_TongHopNhapXuatTheoThangGridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sp_TongHopNhapXuatTheoThangGridControl.Location = new System.Drawing.Point(0, 116);
            this.sp_TongHopNhapXuatTheoThangGridControl.MainView = this.gridView1;
            this.sp_TongHopNhapXuatTheoThangGridControl.MenuManager = this.barManager1;
            this.sp_TongHopNhapXuatTheoThangGridControl.Name = "sp_TongHopNhapXuatTheoThangGridControl";
            this.sp_TongHopNhapXuatTheoThangGridControl.Size = new System.Drawing.Size(1200, 519);
            this.sp_TongHopNhapXuatTheoThangGridControl.TabIndex = 9;
            this.sp_TongHopNhapXuatTheoThangGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // Xfrm_TongHopNhapXuatTheoThang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.HighlightText;
            this.ClientSize = new System.Drawing.Size(1200, 658);
            this.Controls.Add(this.sp_TongHopNhapXuatTheoThangGridControl);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbTenCN);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Xfrm_TongHopNhapXuatTheoThang";
            this.Text = "Xfrm_TongHopNhapXuatTheoThang";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Xfrm_TongHopNhapXuatTheoThang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bds_sp_TongHopNhapXuatTheoThang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sp_TongHopNhapXuatTheoThangGridControl)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cmbTenCN;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem btnPreView;
        private DevExpress.XtraBars.BarButtonItem btnExit;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker dtpTO;
        private System.Windows.Forms.DateTimePicker dtpFROM;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource bds_sp_TongHopNhapXuatTheoThang;
        private DS DS;
        private DSTableAdapters.sp_TongHopNhapXuatTheoThangTableAdapter sp_TongHopNhapXuatTheoThangTableAdapter;
        private DSTableAdapters.TableAdapterManager tableAdapterManager;
        private DevExpress.XtraGrid.GridControl sp_TongHopNhapXuatTheoThangGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colNGAY;
        private DevExpress.XtraGrid.Columns.GridColumn colNHAP;
        private DevExpress.XtraGrid.Columns.GridColumn colTYLENHAP;
        private DevExpress.XtraGrid.Columns.GridColumn colXUAT;
        private DevExpress.XtraGrid.Columns.GridColumn colTYLEXUAT;
    }
}