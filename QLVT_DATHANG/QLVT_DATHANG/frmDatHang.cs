﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG
{
    public partial class frmDatHang : Form
    {
        Boolean flag = false;
        public frmDatHang()
        {
            InitializeComponent();
        }

        private void datHangBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.bdsDH.EndEdit();
            this.tableAdapterManager.UpdateAll(this.DS);

        }

        private void frmDatHang_Load(object sender, EventArgs e)
        {
            
            DS.EnforceConstraints = false;
           
            this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Fill(this.DS.DatHang);


            this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
            this.dsnvTableAdapter.Fill(this.DS.DSNV);

            this.ctdhTableAdapter.Connection.ConnectionString = Program.connstr;
            this.ctdhTableAdapter.Fill(this.DS.CTDDH);

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.vattuTableAdapter.Connection.ConnectionString = Program.connstr;
            this.vattuTableAdapter.Fill(this.DS.Vattu);

            txbMaNV.Text = cmbHoTen.SelectedValue.ToString();
            txtMaKho.Text = cmbKho.SelectedValue.ToString();
            txbMaNV.Enabled = txtMaKho.Enabled = false;
            panelDDH.Enabled = false;

            //ẩn hiện form chi tiết
            btnGhiVT.Enabled = false;

            cmbTenCN.DataSource = Program.bds_dspm;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedIndex = Program.mChinhanh;
        }

        private void btnThem_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            flag = true;
            gcDH.Enabled = false;

            txtMaDH.Enabled = true;
            panelDDH.Enabled = true;
            bdsDH.AddNew();

            btnThem.Enabled = btnSua.Enabled = btnReload.Enabled = btnXoa.Enabled = false;
            btnUndo.Enabled = btnThoat.Enabled = true;
            cmbHoTen.SelectedIndex = 0;
            cmbKho.SelectedIndex = 0;
        }

        private void btnSua_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            flag = false;
            txtMaDH.Enabled = false;
            
            btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = false;
            btnGhi.Enabled = btnReload.Enabled = true;
            panelDDH.Enabled = true;
        }

        private void btnXoa_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            panelDDH.Enabled = false;
            btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = false;

            DialogResult rs = MessageBox.Show("Bạn có chắc muốn xóa ?", "XÓA PHIẾU ĐẶT", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {
                if (bdsCTDH.Count > 0)
                {
                    MessageBox.Show("Bạn không được xóa các đơn đặt hàng đã có chi tiết!", "Thông báo", MessageBoxButtons.OK);

                    bdsDH.Position = 0;
                    panelDDH.Enabled = false;
                    btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = true;
                }
                else
                {
                    try
                    {
                        bdsDH.EndEdit();
                        bdsDH.RemoveCurrent();
                        this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
                        this.datHangTableAdapter.Update(this.DS.DatHang);

                        MessageBox.Show("Bạn đã xóa thành công", "", MessageBoxButtons.OK);
                        bdsDH.Position = 0;
                        panelDDH.Enabled = false;
                        btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = true;

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Lỗi xóa đơn đặt hàng\n" + "Lỗi: " + ex, "", MessageBoxButtons.OK);
                        panelDDH.Enabled = false;
                        btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = true;

                        this.datHangTableAdapter.Fill(this.DS.DatHang);
                        return;
                    }
                }
            }
            else
            {
                panelDDH.Enabled = false;
                btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = btnReload.Enabled = btnUndo.Enabled = true;
            }
        }

        private void btnGhi_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //kiểm tra rỗng và điều kiện input
            if (txtMaDH.Text.Trim() == "")
            {
                MessageBox.Show("Bạn chưa nhập mã đơn hàng!", "THÔNG BÁO", MessageBoxButtons.OK);
                txtMaDH.Focus();
                return;
            }
            if (txtNgay.Text.Trim() == "")
            {
                MessageBox.Show("Bạn chưa chọn ngày lập!", "THÔNG BÁO", MessageBoxButtons.OK);
                txtNgay.Focus();
                return;
            }
            if (txtNCC.Text.Trim() == "")
            {
                MessageBox.Show("Bạn chưa nhập nhà cung cấp!", "THÔNG BÁO", MessageBoxButtons.OK);
                txtNCC.Focus();
                return;
            }

            try
            {

                bdsDH.EndEdit();
                bdsDH.ResetCurrentItem();
                this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.datHangTableAdapter.Update(this.DS.DatHang);

                MessageBox.Show("Ghi thành công!", "THÔNG BÁO", MessageBoxButtons.OK);

                panelDDH.Enabled = false;
                btnSua.Enabled = btnThem.Enabled = btnXoa.Enabled = true;
                btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = true;
                gcDH.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi ghi đơn đặt hàng\n " + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            panelDDH.Enabled = false;
            gcDH.Enabled = true;
            if (Program.mGroup == "CONGTY")
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = false;

            }
            else
            {
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = true;

            }

            bdsDH.CancelEdit();
            bdsDH.EndEdit();
            this.datHangTableAdapter.Fill(DS.DatHang);
        }

        private void btnReload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = panelDDH.Enabled = false;
            this.datHangTableAdapter.Fill(this.DS.DatHang);

            MessageBox.Show("Tải lại dữ liệu thành công", "Thông báo", MessageBoxButtons.OK);


            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = true;
            gcDH.Enabled = true;
            panelDDH.Enabled = false;
            bdsVT.Position = 0;
        }

        private void btnDSDDH_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

        }

        private void btnThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void btnThemVT_Click(object sender, EventArgs e)
        {

            flag = true;
            bdsCTDH.AddNew();

            btnThemVT.Enabled = btnXoaVT.Enabled = btnSuaVT.Enabled = false;
            btnUndoVT.Enabled = btnGhiVT.Enabled = true;

            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = false;
        }

        private void btnXoaVT_Click(object sender, EventArgs e)
        {

            btnThemVT.Enabled = btnXoaVT.Enabled = btnGhiVT.Enabled = btnSuaVT.Enabled = false;

          

            DialogResult rs = MessageBox.Show("Bạn có chắc muốn xóa ?", "XÓA CHI TIẾT ĐƠN ĐẶT HÀNG", MessageBoxButtons.YesNo);
            if (rs == DialogResult.Yes)
            {

                try
                {
                    bdsCTDH.EndEdit();
                    bdsCTDH.RemoveCurrent();
                    this.ctdhTableAdapter.Connection.ConnectionString = Program.connstr;
                    this.ctdhTableAdapter.Update(this.DS.CTDDH);

                    MessageBox.Show("Bạn đã xóa thành công", "", MessageBoxButtons.OK);
                    bdsCTDH.Position = 0;
                    
                    btnThemVT.Enabled = btnXoaVT.Enabled = btnGhiVT.Enabled = btnSuaVT.Enabled = true;

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Lỗi xóa chi tiết đơn\n" + "Lỗi: " + ex, "", MessageBoxButtons.OK);

                    btnThemVT.Enabled = btnXoaVT.Enabled = btnGhiVT.Enabled = btnSuaVT.Enabled = true;

                    this.ctdhTableAdapter.Fill(this.DS.CTDDH);
                    return;
                }

            }
            else
            {
                btnThemVT.Enabled = btnXoaVT.Enabled = btnGhiVT.Enabled = btnSuaVT.Enabled = true;
            }
        }

        private int checkMaVT()
        {
            String querySQL = "SELECT * FROM CTDDH WHERE MasoDDH = '" 
                + ((DataRowView)bdsDH[bdsDH.Position])["MasoDDH"].ToString().Trim() + "' AND MAVT = '" 
                + cTDDHDataGridView.CurrentRow.Cells["MAVT"].Value.ToString() + "'";
            
            Program.myReader = Program.ExecSqlDataReader(querySQL);

            if (Program.myReader.Read() == true)
            {
                MessageBox.Show("Vật tư đã tồn tại", "THÔNG BÁO", MessageBoxButtons.OK);
                
                Program.myReader.Close();
                return 1;
            }
            Program.myReader.Close();
            return 0;
        }

        private void btnGhiVT_Click(object sender, EventArgs e)
        {

            //kiểm tra rỗng và điều kiện input
            if (cTDDHDataGridView.CurrentRow.Cells["MasoDDH"].Value.ToString() != ((DataRowView)bdsDH[bdsDH.Position])["MasoDDH"].ToString())
            {
                MessageBox.Show("Mã đơn đặt phải trùng với đơn mà bạn đang chọn!", "THÔNG BÁO", MessageBoxButtons.OK);
                cTDDHDataGridView.CurrentRow.Cells["MasoDDH"].Value = ((DataRowView)bdsDH[bdsDH.Position])["MasoDDH"].ToString();
                return;
            }
            if (cTDDHDataGridView.CurrentRow.Cells["MAVT"].Value.ToString() == "")
            {
                MessageBox.Show("Bạn chưa chọn vật tư!", "THÔNG BÁO", MessageBoxButtons.OK);
                
                return;
            }
            if(flag == true)
            {
                if (checkMaVT() == 1) return;
            }
            if (cTDDHDataGridView.CurrentRow.Cells["SOLUONG"].Value.ToString() == "" )
            {
                MessageBox.Show("Bạn chưa nhập số lượng!", "THÔNG BÁO", MessageBoxButtons.OK);
                
                return;
            }
            if (cTDDHDataGridView.CurrentRow.Cells["DONGIA"].Value.ToString() == "")
            {
                MessageBox.Show("Bạn chưa nhập đơn giá!", "THÔNG BÁO", MessageBoxButtons.OK);
               
                return;
            }
            try
            {

                bdsCTDH.EndEdit();
                bdsCTDH.ResetCurrentItem();
                this.ctdhTableAdapter.Connection.ConnectionString = Program.connstr;
                this.ctdhTableAdapter.Update(this.DS.CTDDH);

                MessageBox.Show("Ghi thành công!", "THÔNG BÁO", MessageBoxButtons.OK);

                //ẩn hiện form chi tiết
                btnThemVT.Enabled = btnXoaVT.Enabled = btnUndoVT.Enabled = btnSuaVT.Enabled = true;
                btnGhiVT.Enabled = false;
                //ẩn hiện form chính
                btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi ghi đơn đặt hàng\n " + ex.Message, "", MessageBoxButtons.OK);
                return;
            }
        }

        private void btnUndoVT_Click(object sender, EventArgs e)
        {
            //ẩn hiện form chi tiết
            btnThemVT.Enabled = btnXoaVT.Enabled = btnUndoVT.Enabled = btnSuaVT.Enabled = true;
            btnGhiVT.Enabled = false;
            //ẩn hiện form chính
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = true;

            bdsCTDH.CancelEdit();
            bdsCTDH.EndEdit();
            this.ctdhTableAdapter.Fill(DS.CTDDH);
        }

        private void btnSuaVT_Click(object sender, EventArgs e)
        {
            flag = false;
            //ẩn hiện form chi tiết
            btnGhiVT.Enabled = true;
            btnThemVT.Enabled = btnXoaVT.Enabled = false;
            //ẩn hiện form chính
            btnThem.Enabled = btnXoa.Enabled = btnSua.Enabled = btnGhi.Enabled = btnUndo.Enabled = btnReload.Enabled = false;
        }

        private void cmbTenCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTenCN.SelectedValue.ToString() == "System.Data.DataRowView")
            {
                return;
            }
            Program.servername = cmbTenCN.SelectedValue.ToString();

            if (cmbTenCN.SelectedIndex != Program.mChinhanh)
            {
                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }
            if (Program.KetNoi() == 0)
            {
                MessageBox.Show("Lỗi chuyển chi nhánh", "", MessageBoxButtons.OK);
            }
            else
            {
                // TODO: This line of code loads data into the 'DS.DSNV' table. You can move, or remove it, as needed.
                this.dsnvTableAdapter.Connection.ConnectionString = Program.connstr;
                this.dsnvTableAdapter.Fill(this.DS.DSNV);
                // TODO: This line of code loads data into the 'DS.CTDDH' table. You can move, or remove it, as needed.
                this.ctdhTableAdapter.Connection.ConnectionString = Program.connstr;
                this.ctdhTableAdapter.Fill(this.DS.CTDDH);
                // TODO: This line of code loads data into the 'dS.DatHang' table. You can move, or remove it, as needed.
                this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.datHangTableAdapter.Fill(this.DS.DatHang);

                this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
                // TODO: This line of code loads data into the 'DS.DSKHO' table. You can move, or remove it, as needed.
                this.khoTableAdapter.Fill(this.DS.Kho);

                // TODO: This line of code loads data into the 'DS.Vattu' table. You can move, or remove it, as needed.
                this.vattuTableAdapter.Fill(this.DS.Vattu);
                panelDDH.Enabled = false;               
            }
        }
    }
}
