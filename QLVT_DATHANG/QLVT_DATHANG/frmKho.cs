﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG
{
    public partial class frmKho : Form
    {
        String maCN = "";
        Boolean isEdit = true;

        public frmKho()
        {
            InitializeComponent();
        }

        private void khoBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.bdsKho.EndEdit();
            this.tableAdapterManager.UpdateAll(this.DS);

        }

        private void frmKho_Load(object sender, EventArgs e)
        {
            DS.EnforceConstraints = false;

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Fill(this.DS.DatHang);

            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

            maCN = ((DataRowView)bdsKho[0])["MACN"].ToString();

            cmbTenCN.DataSource = Program.bds_dspm;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedItem = Program.mChinhanh;

            if(Program.mGroup == "CONGTY")
            {
                btnAdd.Enabled = btnEdit.Enabled = btnSave.Enabled = btnDelete.Enabled = btnUndo.Enabled = false;
                groupBox1.Enabled = false;
            }
            else
            {
                cmbTenCN.Enabled = false;
                groupBox1.Enabled = false;
            }

        }

        private void cmbTenCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbTenCN.SelectedValue.ToString() == "System.Data.DataRowView")
            {
                return;
            }
            Program.servername = cmbTenCN.SelectedValue.ToString();

            if (cmbTenCN.SelectedIndex != Program.mChinhanh)
            {
                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }

            if(Program.KetNoi() == 0)
            {
                MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
            }
            else
            {
                this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
                this.khoTableAdapter.Fill(this.DS.Kho);

                this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.datHangTableAdapter.Fill(this.DS.DatHang);

                this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
                this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

                this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
                this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

                maCN = ((DataRowView)bdsKho[0])["MACN"].ToString();
                txbMaCN.Text = maCN;
            }
        }

        private void btnAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            isEdit = false;
            txbMaKho.Enabled = true;
            gcKho.Enabled = false;
            groupBox1.Enabled = true;
            btnDelete.Enabled = btnEdit.Enabled = btnReLoad.Enabled = btnExit.Enabled = false;
            btnSave.Enabled = btnUndo.Enabled = true;

            bdsKho.AddNew();
            txbMaCN.Text = maCN;
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            isEdit = true;
            txbMaKho.Enabled = false;
            groupBox1.Enabled = true;
            btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = false;
            btnSave.Enabled = btnUndo.Enabled = true;
        }

        private int sp_TimKho()
        {
            String sp_TimKho = "EXEC sp_TimKho '" + txbMaKho.Text.Trim() + "'";
            Program.myReader = Program.ExecSqlDataReader(sp_TimKho);
            if (Program.myReader != null)
            {
                MessageBox.Show("Mã kho này đã tồn tại !", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaKho.Focus();
                Program.myReader.Close();
                return 1;
            }
            return 0;
        }

        private void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            if (txbMaKho.Text.Trim() == "")
            {
                MessageBox.Show("Mã Kho không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaKho.Focus();
                return;
            }


            ////kiểm tra trùng mã
            if (!isEdit)
            {
                if(sp_TimKho() == 1)
                {
                    return;
                }
            }


            if (txbTenKho.Text.Trim() == "")
            {
                MessageBox.Show("Tên Kho không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                txbTenKho.Focus();
                return;
            }

            if (txbDiaChi.Text.Trim() == "")
            {
                MessageBox.Show("Đại chỉ không được để trống!", "THÔNG BÁO", MessageBoxButtons.OK);
                txbDiaChi.Focus();
                return;
            }

            try
            {
                
                bdsKho.EndEdit();
                bdsKho.ResetCurrentItem();
                this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
                this.khoTableAdapter.Update(this.DS.Kho);

                MessageBox.Show("Ghi thành công!", "THÔNG BÁO", MessageBoxButtons.OK);

                groupBox1.Enabled = false;
                btnAdd.Enabled = btnEdit.Enabled = btnSave.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                gcKho.Enabled = true;
                isEdit = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi ghi \nLoi: " + ex, "THÔNG BÁO", MessageBoxButtons.OK);
                groupBox1.Enabled = false;
                btnAdd.Enabled = btnEdit.Enabled = btnSave.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                gcKho.Enabled = true;
                isEdit = true;
                return;
            }

        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DialogResult rs = MessageBox.Show("Bạn có muốn xóa không ? ", "THÔNG BÁO", MessageBoxButtons.YesNo);

            if(rs == DialogResult.Yes)
            {
                if(bdsDH.Count > 0 || bdsPN.Count > 0 || bdsPX.Count > 0)
                {
                    MessageBox.Show("Không được xóa Kho này !", "THÔNG BÁO", MessageBoxButtons.OK);
                    bdsKho.Position = 0;
                    groupBox1.Enabled = false;
                    btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = true;
                }
                else
                {
                    try
                    {
                        bdsKho.EndEdit();
                        bdsKho.RemoveCurrent();
                        this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
                        this.khoTableAdapter.Update(this.DS.Kho);
                        
                        bdsKho.Position = 0;
                        groupBox1.Enabled = false;
                        btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Lỗi xóa \nLỗi: " + ex, "THÔNG BÁO", MessageBoxButtons.OK);

                        groupBox1.Enabled = false;
                        btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = true;
                        this.khoTableAdapter.Fill(this.DS.Kho);
                    }
                }
            }
        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            groupBox1.Enabled = false;
            if (Program.mGroup == "CONGTY")
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = false;
                cmbTenCN.Enabled = true;
                gcKho.Enabled = true;
                isEdit = true;
            }
            else
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = true;
                cmbTenCN.Enabled = false;
                gcKho.Enabled = true;
                isEdit = true;
            }

            bdsKho.CancelEdit();
            bdsKho.EndEdit();
            this.khoTableAdapter.Fill(DS.Kho);

        }

        private void btnReLoad_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnExit.Enabled = groupBox1.Enabled = false;

            this.khoTableAdapter.Connection.ConnectionString = Program.connstr;
            this.khoTableAdapter.Fill(this.DS.Kho);

            this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Fill(this.DS.DatHang);

            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

            MessageBox.Show("Tải lại dữ liệu thành công", "Thông báo", MessageBoxButtons.OK);


            btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
            groupBox1.Enabled = false;
            bdsKho.Position = 0;
            gcKho.Enabled = true;
            isEdit = true;
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }
    }
}
