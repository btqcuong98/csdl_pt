﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG
{
    public partial class frmDangNhap : Form
    {
        public frmDangNhap()
        {
            InitializeComponent();
        }

        private void frmDangNhap_Load(object sender, EventArgs e)
        {
            string chuoiketnoi = "Data Source=BUICUONG;Initial Catalog=QLVT_DATHANG;Integrated Security=True";
            Program.conn.ConnectionString = chuoiketnoi;
            Program.conn.Open();
            DataTable dt = new DataTable();
            dt = Program.ExecSqlDataTable("SELECT * FROM V_DS_PHANMANH");
            //GIỮ DATA
            Program.bds_dspm.DataSource = dt;

            //LẤY DATA HIỂN THỊ RA COMBOBOX
            cmbTenCN.DataSource = dt;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedIndex = -1;
            cmbTenCN.SelectedIndex = 0;
        }

        private void cmbTenCN_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //GIỮ TÊN SERVER
                Program.servername = cmbTenCN.SelectedValue.ToString();

                //HIỂN THỊ TÊN SERVER LÊN BÊN CẠNH COMBOBOX
                txbServerName.Text = Program.servername;
            }
            catch (Exception) { };
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txbUserName.Text.Trim() == "" || txbPassWord.Text.Trim() == "")
            {
                MessageBox.Show("Login name và mật mã không được trống", "", MessageBoxButtons.OK);
                return;
            }
            Program.mlogin = txbUserName.Text; Program.password = txbPassWord.Text;
            if (Program.KetNoi() == 0) return;

            Program.mChinhanh = cmbTenCN.SelectedIndex;
            Program.mloginDN = Program.mlogin;
            Program.passwordDN = Program.password;

            string strLenh = "EXEC SP_DANGNHAP '" + Program.mlogin + "'";
            Program.myReader = Program.ExecSqlDataReader(strLenh);
           
            if (Program.myReader == null) return;
            Program.myReader.Read();

            Program.username = Program.myReader.GetString(0); // Lay IDNV -> USERNAME
            if (Convert.IsDBNull(Program.username))
            {
                MessageBox.Show("Login bạn nhập không có quyền truy cập dữ liệu\n Bạn xem lại username, password", "", MessageBoxButtons.OK);
                return;
            }
            Program.mHoten = Program.myReader.GetString(1);
            Program.mGroup = Program.myReader.GetString(2);
            Program.myReader.Close();
            Program.conn.Close();

            


            frmMain f = new frmMain();
            //this.Hide();
            f.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
