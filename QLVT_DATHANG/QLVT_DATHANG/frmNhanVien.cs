﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLVT_DATHANG
{
    public partial class frmNhanVien : Form
    {
        String maCN = "";
        Boolean isEdit = true;

        public frmNhanVien()
        {
            InitializeComponent();
        }

        private void nhanVienBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.bdsNV.EndEdit();
            this.tableAdapterManager.UpdateAll(this.DS);

        }

        private void frmNhanVien_Load(object sender, EventArgs e)
        {
            DS.EnforceConstraints = false;

            this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
            this.nhanVienTableAdapter.Fill(this.DS.NhanVien);
            
            this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Fill(this.DS.DatHang);
            
            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);
           
            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

            maCN = ((DataRowView)bdsNV[0])["MACN"].ToString();

            cmbTenCN.DataSource = Program.bds_dspm;
            cmbTenCN.DisplayMember = "TENCN";
            cmbTenCN.ValueMember = "TENSERVER";
            cmbTenCN.SelectedIndex = Program.mChinhanh;

            groupBox1.Enabled = false;
            if (Program.mGroup == "CONGTY")
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = false;
            }
            else
            {
                cmbTenCN.Enabled = false;
            }

            if(Program.mGroup == "CHINHANH")
            {
                btnMove.Enabled = true;
            }
            else
            {
                btnMove.Enabled = false;
            }
            
        }

        private void cmbTenCN_SelectedIndexChanged_1(object sender, EventArgs e)
        {

            if (cmbTenCN.SelectedValue.ToString() == "System.Data.DataRowView")
            {
                return;
            }
            Program.servername = cmbTenCN.SelectedValue.ToString();

            if (cmbTenCN.SelectedIndex != Program.mChinhanh)
            {

                Program.mlogin = Program.remotelogin;
                Program.password = Program.remotepassword;
            }
            else
            {
                Program.mlogin = Program.mloginDN;
                Program.password = Program.passwordDN;
            }

            if (Program.KetNoi() == 0)
            {
                MessageBox.Show("Lỗi kết nối về chi nhánh mới", "", MessageBoxButtons.OK);
            }
            else
            {
                this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                this.nhanVienTableAdapter.Fill(this.DS.NhanVien);

                this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
                this.datHangTableAdapter.Fill(this.DS.DatHang);

                this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
                this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

                this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
                this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

                maCN = ((DataRowView)bdsNV[0])["MACN"].ToString();
                txbMaCN.Text = maCN;
            }
        }

        private void btnAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            isEdit = false;
            txbMaNV.Enabled = true;
            groupBox1.Enabled = true;
            gvNV.Enabled = false;
            bdsNV.AddNew();
            txbMaCN.Text = maCN;
            txbTTXOA.Enabled = false;
            txbTTXOA.Text = "0";

            btnAdd.Enabled = false;
            btnDelete.Enabled = btnEdit.Enabled  = btnReLoad.Enabled = btnMove.Enabled = btnExit.Enabled = false;
            btnSave.Enabled = btnUndo.Enabled = true;
        }

        private void btnEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            isEdit = true;
            txbMaNV.Enabled = false;
            groupBox1.Enabled = true;
            btnEdit.Enabled = false;
            btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnMove.Enabled = btnExit.Enabled = false;
            btnSave.Enabled = btnUndo.Enabled = true;
        }

        private int sp_TimNV ()
        {
            String sp_TimNV = "EXEC sp_TimNV '" + txbMaNV.Text.Trim() + "'";
            Program.myReader = Program.ExecSqlDataReader(sp_TimNV);
            if (Program.myReader != null)
            {
                MessageBox.Show("Mã nhân viên đã tồn tại", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaNV.Focus();
                Program.myReader.Close();
                return 1;
            }
            return 0;
        }

        private void btnSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if(txbMaNV.Text.Trim() == "")
            {
                MessageBox.Show("Mã nhân viên không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbMaNV.Focus();
                return;
            }

            //Kiểm tra trùng mã
            if (!isEdit)
            {
                if (sp_TimNV() == 1)
                {
                    return;
                }
            }


            if (txbHO.Text.Trim() == "")
            {
                MessageBox.Show("Họ không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbHO.Focus();
                return;
            }

            if (txbTEN.Text.Trim() == "")
            {
                MessageBox.Show("Tên không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbTEN.Focus();
                return;
            }

            if (dptNGAYSINH.Text.Trim() == "")
            {
                MessageBox.Show("Ngày sinh không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                dptNGAYSINH.Focus();
                return;
            }

            if (txbLUONG.Text.Trim() == "")
            {
                MessageBox.Show("Lương không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbLUONG.Focus();
                return;
            }

            if (float.Parse(txbLUONG.Text.ToString().Replace(",","")) < 4000000)
            {
                MessageBox.Show("Lương cơ bản 4.000.000", "THÔNG BÁO", MessageBoxButtons.OK);
                txbLUONG.Focus();
                return;
            }

            if (txbDIACHI.Text.Trim() == "")
            {
                MessageBox.Show("Địa chỉ không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbDIACHI.Focus();
                return;
            }

            if (txbTTXOA.Text.Trim() == "")
            {
                MessageBox.Show("Trạng thái xóa không được để trống", "THÔNG BÁO", MessageBoxButtons.OK);
                txbTTXOA.Focus();
                return;
            }

            if((txbTTXOA.Text != "0") && (txbTTXOA.Text != "1"))
            {
                MessageBox.Show("Trạng thái không hợp lệ (0: đang làm việc || 1: Xóa) ", "THÔNG BÁO", MessageBoxButtons.OK);
                txbTTXOA.Focus();
                return;
            }

            try
            {
                bdsNV.EndEdit();
                bdsNV.ResetCurrentItem();
                this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                this.nhanVienTableAdapter.Update(this.DS.NhanVien);

                MessageBox.Show("Ghi thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                groupBox1.Enabled = false;
                btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = btnExit.Enabled = true;
                if (Program.mGroup == "CHINHANH"){ btnMove.Enabled = true;} else { btnMove.Enabled = false;}
                gvNV.Enabled = true;
                isEdit = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi ghi nhân viên\n " + ex.Message, "", MessageBoxButtons.OK);
                groupBox1.Enabled = false;
                btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled =  btnExit.Enabled = true;
                if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
                gvNV.Enabled = true;
                isEdit = true;
                return;
            }

        }

        private void btnDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            groupBox1.Enabled = false;
            btnSave.Enabled = btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnMove.Enabled = btnExit.Enabled = false;
            if (txbMaNV.Text.Trim() == Program.username)
            {
                MessageBox.Show("Tài khoản này đang được sử dụng !", "THÔNG BÁO", MessageBoxButtons.OK);
                groupBox1.Enabled = false;
                btnEdit.Enabled = btnAdd.Enabled = btnSave.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = true;
                if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
                return;
            }
            DialogResult rs = MessageBox.Show("Bạn có chắc muốn xóa ?", "XÓA NHÂN VIÊN", MessageBoxButtons.YesNo);

            if(rs == DialogResult.Yes)
            {
                if(bdsDH.Count > 0 || bdsPN.Count > 0 || bdsPX.Count > 0)
                {
                    DialogResult rs2 = MessageBox.Show("Lo lắng: Nhân viên đã lập phiếu \nBạn vẫn muốn xóa ?", "THÔNG BÁO", MessageBoxButtons.YesNo);

                    if(DialogResult.Yes == rs2)
                    {
                        try
                        {
                            txbTTXOA.Text = "1";
                            bdsNV.EndEdit();
                            bdsNV.ResetCurrentItem();
                            this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                            this.nhanVienTableAdapter.Update(this.DS.NhanVien);

                            MessageBox.Show("Xóa thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

                            bdsNV.Position = 0;
                            groupBox1.Enabled = false;
                            btnSave.Enabled = btnEdit.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = btnExit.Enabled = true;
                            if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Lỗi xóa nhân viên\n " + "Lỗi:\n" + ex.Message, "", MessageBoxButtons.OK);
                            groupBox1.Enabled = false;
                            btnEdit.Enabled = btnAdd.Enabled = btnSave.Enabled = btnDelete.Enabled =  btnReLoad.Enabled = btnUndo.Enabled = true;
                            if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
                            this.nhanVienTableAdapter.Fill(this.DS.NhanVien);
                            return;
                        }
                    }
                    else
                    {
                        bdsNV.Position = 0;
                        groupBox1.Enabled = false;
                        btnEdit.Enabled = btnAdd.Enabled = btnSave.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled  = true;
                        if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
                    } 
                }
                else
                {
                    try
                    {
                        bdsNV.EndEdit();
                        bdsNV.RemoveCurrent();
                        this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
                        this.nhanVienTableAdapter.Update(this.DS.NhanVien);

                        MessageBox.Show("Xóa thành công !","THÔNG BÁO", MessageBoxButtons.OK);

                        bdsNV.Position = 0;
                        groupBox1.Enabled = false;
                        btnEdit.Enabled = btnAdd.Enabled = btnSave.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = true;
                        if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Lỗi xóa nhân viên\n " + "Lỗi:\n" + ex.Message, "", MessageBoxButtons.OK);
                        groupBox1.Enabled = false;
                        btnEdit.Enabled = btnAdd.Enabled = btnSave.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled = true;
                        if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
                        this.nhanVienTableAdapter.Fill(this.DS.NhanVien);
                        return;
                    }
                }
                
            }
            else
            {
                groupBox1.Enabled = true;
                btnEdit.Enabled = btnAdd.Enabled = btnSave.Enabled = btnDelete.Enabled = btnReLoad.Enabled = btnUndo.Enabled  = true;
                if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
            }

        }

        private void btnUndo_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            groupBox1.Enabled = false;
            if (Program.mGroup == "CONGTY")
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnMove.Enabled = false;
                cmbTenCN.Enabled = true;
                gvNV.Enabled = true;
                isEdit = true;
            }
            else
            {
                btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnReLoad.Enabled = btnMove.Enabled = btnExit.Enabled = true;
                cmbTenCN.Enabled = false;
                gvNV.Enabled = true;
                isEdit = true;
            }

            bdsNV.CancelEdit();
            bdsNV.EndEdit();
            this.nhanVienTableAdapter.Fill(DS.NhanVien);

        }

        private void btnReload_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            btnReLoad.Enabled =  btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnMove.Enabled = groupBox1.Enabled = false;

            this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
            this.nhanVienTableAdapter.Fill(this.DS.NhanVien);

            this.datHangTableAdapter.Connection.ConnectionString = Program.connstr;
            this.datHangTableAdapter.Fill(this.DS.DatHang);

            this.phieuNhapTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuNhapTableAdapter.Fill(this.DS.PhieuNhap);

            this.phieuXuatTableAdapter.Connection.ConnectionString = Program.connstr;
            this.phieuXuatTableAdapter.Fill(this.DS.PhieuXuat);

            MessageBox.Show("Tải lại dữ liệu thành công", "Thông báo", MessageBoxButtons.OK);


            btnReLoad.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnMove.Enabled = btnExit.Enabled = true;
            if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
            groupBox1.Enabled = false;
            bdsNV.Position = 0;
            gvNV.Enabled = true;
            isEdit = true;
        }

        private void btnMove_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            
            String CN = "";
            if(txbMaNV.Text.Trim() == Program.username)
            {
                MessageBox.Show("Tài khoản này đang được sử dụng !", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }
            if(Int32.Parse(txbTTXOA.Text) == 1)
            {
                MessageBox.Show("Nhân viên này không còn khả dụng !", "THÔNG BÁO", MessageBoxButtons.OK);
                return;
            }

            btnReLoad.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnMove.Enabled = groupBox1.Enabled = btnExit.Enabled = false;

            if (Program.mChinhanh == 0)
            {
                CN = "CN2";
            }
            else
            {
                CN = "CN1";
            }

            String sp_chuyenCN = "EXEC sp_ChuyenCN " + txbMaNV.Text.Trim() + ",'" +txbHO.Text.Trim() +"','" +txbTEN.Text.Trim()
                                    +"','" +txbDIACHI.Text.Trim() +"','" +dptNGAYSINH.Text.Trim() 
                                    +"'," + float.Parse(txbLUONG.Text.ToString().Replace(",", "")) + ",'"+CN+"'";
            Program.myReader = Program.ExecSqlDataReader(sp_chuyenCN);
            if(Program.myReader == null)
            {
                MessageBox.Show("Chuyển chi nhánh thất bại !", "THÔNG BÁO", MessageBoxButtons.OK);
                btnReLoad.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnMove.Enabled = btnExit.Enabled = true;
                if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
                groupBox1.Enabled = false;
                bdsNV.Position = 0;
                gvNV.Enabled = true;
                return;
            }
            MessageBox.Show("Chuyển chi nhánh thành công !", "THÔNG BÁO", MessageBoxButtons.OK);

            btnReLoad.Enabled = btnAdd.Enabled = btnDelete.Enabled = btnEdit.Enabled = btnSave.Enabled = btnUndo.Enabled = btnMove.Enabled = btnExit.Enabled = true;
            if (Program.mGroup == "CHINHANH") { btnMove.Enabled = true; } else { btnMove.Enabled = false; }
            groupBox1.Enabled = false;
            bdsNV.Position = 0;
            gvNV.Enabled = true;
            //tải lại
            this.nhanVienTableAdapter.Connection.ConnectionString = Program.connstr;
            this.nhanVienTableAdapter.Fill(this.DS.NhanVien);
        }

        private void btnExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }
    }
}
